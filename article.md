---
title: "Podívejte se, kdo zvolil vašeho senátora. Přinášíme výsledky druhého kola okrsek po okrsku"
perex: "Kdyby se volilo jen v Šardicích, Nechvalíně či Rohatci, nemusel se Zdeněk Škromach (ČSSD) po dvaceti letech loučit s místem v Parlamentu. Ukazuje to interaktivní mapa, kam Český rozhlas zakreslil výsledek druhého kola senátních voleb v každém jednotlivém okrsku. Prozkoumejte ji a zjistíte, jak a kde uspěli všichni kandidáti do Senátu."
description: "Interaktivní mapa, kam Český rozhlas zakreslil výsledek druhého kola senátních voleb v každém jednotlivém okrsku. Prozkoumejte ji a zjistíte, jak a kde uspěli všichni kandidáti do Senátu."
authors: ["Jan Cibulka", "Michal Zlatkovský", "Petr Kočí"]
published: "17. října 2016"
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
url: "senatni-volby-v-okrscich"
socialimg: https://interaktivni.rozhlas.cz/senatni-volby-v-okrscich/media/socimg.png
libraries: [jquery, "https://api.tiles.mapbox.com/mapbox-gl-js/v0.25.1/mapbox-gl.js", "https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v1.3.1/mapbox-gl-geocoder.js", "https://select2.github.io/dist/js/select2.full.js"]
styles: ["https://api.tiles.mapbox.com/mapbox-gl-js/v0.25.1/mapbox-gl.css", "https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v1.3.1/mapbox-gl-geocoder.css", "https://select2.github.io/dist/css/select2.min.css"]
recommended:
  - link: https://interaktivni.rozhlas.cz/zisky-v-krajskych-volbach/
    title: "Mapa: ČSSD ztrácela napříč republikou, ANO triumfuje"
    perex: "Srovnání zisků jednotlivých stran v letošních krajských volbách ukazuje, že voliči odcházejí od sociálních demokratů k ANO 2011 Andreje Babiše."
    image: https://interaktivni.rozhlas.cz/data/kraj-volby-zisky/www/media/socialimg.jpg
  - link: https://interaktivni.rozhlas.cz/preference-2016/
    title: "Šampioni preferenčních hlasů: Kdo nejvíc získal, kdo nejvíc ztratil?"
    perex: "Sedmačtyřicet nově zvolených krajských zastupitelů získalo mandát jen díky „kroužkování“. Jiných sedmačtyřicet kandidátů ze stejného důvodu o místo v zastupitelstvu přišlo. Podívejte se, kteří to jsou a kterými kandidátkami preferenční hlasy zamíchaly nejvíce."
    image: https://interaktivni.rozhlas.cz/data/volby-2016-preference-text/www/media/socialimg.jpg
  - link: https://interaktivni.rozhlas.cz/krajske-volby-v-okrscich/
    title: Jak volili vaši sousedi? Prohlédněte si nejpodrobnější mapu volebních výsledků
    perex: Z podrobné mapy Českého rozhlasu  můžete zjistit, které obce se nejvíce vzepřely celkovým výsledkům, kde mají jednotlivé strany největší podporu i jak volili vaši sousedé.
    image: https://interaktivni.rozhlas.cz/krajske-volby-v-okrscich/media/socimg.jpg
---

Zdeněk Škromach ovšem co do počtu získaných okrsků není největším poraženým. Vítězství kandidáta starostů Michala Canova v Liberci bylo tak drtivé, že jeho soupeř, majitel pivovaru ve Frýdlantu Marek Vávra (za ANO), mu dokázal vzdorovat jen ve dvou sídlištních okrscích.

Filozof Václav Bělohradský, který v Praze 6 kandidoval za ČSSD a zelené proti řediteli gymnázia Jiřímu Růžičkovi (za TOP 09), zvítězil dokonce jen v jediném okrsku - a navíc o pouhých 26 hlasů. V řadě dalších senátních obvodů byly ale souboje poměrně vyrovnané.

<aside class="big">
<div id="mapa">
  <div id='info'>Najetím na mapu vyberte okrsek.</div>
  <div id='map'></div>
</div>
</aside>

_Zdroj: [Volby.cz](http://volby.cz)_

Mapu výsledků senátních voleb ve všech okrscích připravil Český rozhlas týden [po obdobné mapě, která ukazovala výsledky krajských voleb](https://interaktivni.rozhlas.cz/krajske-volby-v-okrscich/) a vzbudila [ohlas na sociálních sítích](https://twitter.com/search?f=tweets&vertical=default&q=https%3A%2F%2Finteraktivni.rozhlas.cz%2Fkrajske-volby-v-okrscich%2F&src=typd).

Je to nejpodrobnější mapa, jakou lze vůbec na základě volebních výsledků nakreslit. Nejsou v ní vidět jen výsledky v jednotlivých obcích, kterých máme v Česku něco šest tisíc, ale i ve všech volebních okrscích, kterých je ještě o jeden řád víc.

Čtenáři si mohou najít vlastní okrsek nebo prozkoumat okrsky ve svém okolí. Uvidí i výkyvy způsobené například tím, že v jednom bloku na sídlišti bydlí zaměstnanci jedné firmy či instituce, kteří volí podobným způsobem, nebo většinu obyvatel okrsku tvoří lidé z domova důchodců – a ti také volí specificky.